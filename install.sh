#!/usr/bin/env bash

ROOT_UID=0
DEST_DIR=
THEME_NAME=Papirus-Mace

# Destination directory
if [ "$UID" -eq "$ROOT_UID" ]; then
  DEST_DIR="/usr/share"
else
  DEST_DIR="$HOME/.local/share"
fi

copy_variant(){
mkdir -p $DEST_DIR/color-schemes
cp color-schemes/* $DEST_DIR/color-schemes/
mkdir -p $DEST_DIR/konsole
cp konsole/* $DEST_DIR/konsole/
mkdir -p $DEST_DIR/plasma/desktoptheme
cp -r desktoptheme/* $DEST_DIR/plasma/desktoptheme/
mkdir -p $DEST_DIR/plasma/look-and-feel
cp -r look-and-feel/* $DEST_DIR/plasma/look-and-feel/
}

cd "$(dirname "$0")"
copy_variant
echo
echo "Themes installed to $DEST_DIR"
echo

exit
